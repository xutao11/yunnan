<?php
/**
 * Created by PhpStorm.
 * User: PHP
 * Date: 2018/1/4
 * Time: 14:51
 */
namespace app\index\controller;

use app\index\model\Member;
use think\Controller;
use think\Db;

class Base extends Controller
{
    protected $user = [];

    /**
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @author   [小码哥]
     */
    public function _initialize()
    {
        /* =========================== 加载配置 =================================== */
        $config = cache('db_config_data');
        if(!$config){
            $config = load_config();
            cache('db_config_data', $config);
        }
        config($config);
        /* =========================== 加载配置 END =================================== */
        $openid = session('openid');
        if(!cache($openid)||!$openid){
            $this->redirect(url('index/sign/sign_in'));
        }
        $openid = session('openid');
        if(!cache('access_token')){
            $this->redirect(url('index/sign/sign_in'));
        }
        cache($openid, 1,(60*60*24));
        $member = Db::name('Member')
            ->alias('a')
            ->join(['__MEMBER_GROUP__'=>'b'],'a.group_id=b.id','left')
            ->field(['a.*','b.group_name'])
            ->where(['openid' => $openid])
            ->find();
        session('member', $member);
        $this->user = $member;
        $this->assign('user', $member);
        if(!$member['name']){
            $this->redirect(url('index/sign/step_one'));
        }

    }
}