<?php
/**
 * Created by PhpStorm.
 * User: PHP
 * Date: 2018/2/3
 * Time: 11:00
 */
namespace app\index\controller;
class Order extends Base
{
    /**
     * 订单列表
     *
     * @return mixed|\think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @author   [小码哥]
     */
    public function index()
    {
        $status = input('status', '');
        $shop   = get_or_set_shop('get', 'shop');
        if(request()->isPost()){
            $where = [];
            if(!empty($status)){
                $where['status'] = ['IN', str_replace('_', ',', $status)];
            }
            $page  = input('page', 1);
            $limit = input('limit', 10);
            $Order = new \app\index\model\Order();
            $data  = $Order->getOrderAll($where, $page, $limit);
            $html  = $this->fetch('index_ajax', ['data' => $data, 'status' => $status, 'shop' => $shop]);
            return json(['code' => 200, 'data' => $html, 'msg' => '订单列表']);
        }
        $html = $this->fetch('index_ajax', ['status' => $status, 'shop' => $shop]);
        return $this->fetch('index', ['status' => $status, 'shop' => $shop, 'data' => $html]);
    }
}