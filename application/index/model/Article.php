<?php
/**
 * Created by PhpStorm.
 * User: PHP
 * Date: 2018/1/24
 * Time: 16:42
 */
namespace app\index\model;

use think\Model;

class Article extends Model
{
    /**
     * 获取新闻列表
     *
     * @param array $where
     *
     * @return false|\PDOStatement|string|\think\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @author   [小码哥]
     */
    public function getList($where = [])
    {
        return $this->where($where)->order(['create_time' => 'desc'])->select();
    }

    /**
     * @param $id
     *
     * @return array|false|\PDOStatement|string|Model
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @author   [小码哥]
     */
    public function getItem($id)
    {
        return $this->where('id', $id)->find();
    }

    /**
     * 地址格式化
     *
     * @param $value
     *
     * @return mixed
     * @author   [小码哥]
     */
    public function getPhotoAttr($value)
    {
        return str_replace('\\', '/', $value);
    }
}