<?php
/**
 * Created by PhpStorm.
 * User: PHP
 * Date: 2018/2/6
 * Time: 16:47
 */
namespace app\index\model;

use think\Model;

/**
 * 商品类
 * Class Goods
 *
 * @package app\index\model
 */
class Goods extends Model
{
    /**
     * 获取商品列表
     *
     * @param array $where
     * @param int   $page
     * @param int   $limit
     *
     * @return false|\PDOStatement|string|\think\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @author   [小码哥]
     */
    public function getGoods($where = [], $page = 1, $limit = 10)
    {
        return $this->where($where)->page($page, $limit)->order(['create_time'=>'DESC'])->select();
    }

    /**
     * 获取单个商品详细信息
     *
     * @param int $goods_id
     *
     * @return array|false|\PDOStatement|string|Model
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @author   [小码哥]
     */
    public function getGoodsItem($goods_id = 0)
    {
        $result          = $this->where('goods_id', $goods_id)->find();
        $result['thumb'] = (new GoodsPic())->getGoodsPic(['goods_id' => $goods_id]);
        return $result;
    }


}