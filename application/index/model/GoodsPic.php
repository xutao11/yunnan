<?php
/**
 * Created by PhpStorm.
 * User: PHP
 * Date: 2018/2/6
 * Time: 16:50
 */
namespace app\index\model;

use think\Model;

class GoodsPic extends Model
{
    /**
     * 获取商品缩略图列表
     *
     * @param array $where
     *
     * @return false|\PDOStatement|string|\think\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @author   [小码哥]
     */
    public function getGoodsPic($where = [])
    {
        return $this->where($where)->select();
    }
}