<?php
/**
 * Created by PhpStorm.
 * User: PHP
 * Date: 2018/2/7
 * Time: 10:43
 */
namespace app\index\model;

use think\Model;

class Order extends Model
{
    /**
     * 获取订单列表
     *
     * @param array $where
     * @param int   $page
     * @param int   $limit
     *
     * @return false|\PDOStatement|string|\think\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @author   [小码哥]
     */
    public function getOrderAll($where = [], $page = 1, $limit = 10)
    {
        return $this->where($where)->page($page, $limit)->order(['create_time' => 'DESC', 'status' => 'ASC'])->select();
    }
}