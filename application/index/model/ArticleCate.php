<?php
/**
 * Created by PhpStorm.
 * User: PHP
 * Date: 2018/1/24
 * Time: 16:43
 */
namespace app\index\model;

use think\Model;

class ArticleCate extends Model
{
    /**
     * 获取新闻分类
     *
     * @return false|\PDOStatement|string|\think\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @author   [小码哥]
     */
    public function getCate()
    {
        $cate = $this->where(['status' => 1])->order(['orderby' => 'ASC'])->select();
        foreach($cate as $key => $value){
            if($key === 0){
                $cate[$key]['child'] = (new Article())->getList(['cate_id' => $value['id'],'status'=>1]);
            }
        }
        return $cate;
    }
}