<?php
/**
 * Created by PhpStorm.
 * User: PHP
 * Date: 2018/2/8
 * Time: 12:44
 */
namespace app\index\model;

use think\exception\PDOException;
use think\Model;

class Address extends Model
{
    /**
     * 收货地址列表
     *
     * @param array $where
     *
     * @return false|\PDOStatement|string|\think\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @author   [小码哥]
     */
    public function getAddressAll($where = [])
    {
        return $this->where($where)->select();
    }

    /**
     * 获取单条收货地址
     *
     * @param int $id
     *
     * @return array|false|\PDOStatement|string|Model
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @author   [小码哥]
     */
    public function getAddressOn($id = 0)
    {
        return $this->where('id', $id)->find();
    }

    /**
     * 添加收货地址
     *
     * @param array $param
     *
     * @return array
     * @author   [小码哥]
     */
    public function addAddress($param = [])
    {
        try{
            $response = $this->allowField(true)->save($param);
            if($response){
                return ['code' => 200, 'data' => null, 'msg' => '添加成功'];
            }
            return ['code' => 0, 'data' => null, 'msg' => '添加失败'];
        }catch(PDOException $e){
            return ['code' => 0, 'data' => null, 'msg' => $e->getMessage()];
        }
    }

    /**
     * 编辑收货地址
     *
     * @param array $param
     *
     * @return array
     * @author   [小码哥]
     */
    public function editAddress($param = [])
    {
        try{
            $response = $this->allowField(true)->save($param, ['id' => $param['id']]);
            if($response){
                return ['code' => 200, 'data' => null, 'msg' => '编辑成功'];
            }
            return ['code' => 0, 'data' => null, 'msg' => '编辑失败'];
        }catch(PDOException $e){
            return ['code' => 0, 'data' => null, 'msg' => $e->getMessage()];
        }
    }

    /**
     * 设置默认收货地址
     *
     * @param int $id
     *
     * @return array
     * @author   [小码哥]
     */
    public function defaultAddress($id = 0)
    {
        $item = $this->where('id', $id)->find();
        try{
            $this->where(['user_id' => $item['user_id'], 'id' => ['<>', $id]])->setField(['default' => 0]);
            $result = $this->where('id', $id)->setField(['default' => $item['default'] == 0 ? 1 : 0]);
            if($result){
                return ['code' => 200, 'data' => null, 'msg' => '设置成功'];
            }
            return ['code' => 0, 'data' => null, 'msg' => '设置失败'];
        }catch(PDOException $e){
            return ['code' => 0, 'data' => null, 'msg' => $e->getMessage()];
        }
    }

    /**
     * 删除收货地址
     *
     * @param int $id
     *
     * @return array
     * @author   [小码哥]
     */
    public function delAddress($id = 0)
    {
        $response = $this->where('id', $id)->delete();
        if($response){
            return ['code' => 200, 'data' => null, 'msg' => '删除成功'];
        }
        return ['code' => 0, 'data' => null, 'msg' => '删除失败'];
    }
}