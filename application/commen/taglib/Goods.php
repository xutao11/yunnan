<?php
/**
 * Created by PhpStorm.
 * User: PHP
 * Date: 2018/2/4
 * Time: 15:28
 */
namespace app\commen\taglib;

use think\Db;
use think\template\TagLib;

class Goods extends TagLib
{
    /**
     * 定义标签列表
     */
    protected $tags = [
        // 标签定义： attr 属性列表 close 是否闭合（0 或者1 默认1） alias 标签别名 level 嵌套层次
        'list' => ['attr' => 'where,name', 'close' => 0] //闭合标签，默认为不闭合
    ];

    /**
     * 商品
     *
     * @param $tag
     *
     * @return string
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @author   [小码哥]
     */
    public function tagList($tag)
    {
        $list = Db::name('Goods')->where($tag['where'])->select();
        $html = '<?php';
        foreach($list as $key => $value){
            $goods_price = sprintf('%.2f', $value['goods_price']);
            $html        .= "<li>
                                <a href='" . url('index/product_details', ['goods_id' => $value['goods_id']]) . "'>
                                    <div>
                                        <img data-lazyload='/static/uploads/goods/{$value['goods_photo']}' alt=''>
                                    </div>
                                    <p class='title'>{$value['goods_name']}</p>
                                    <p class='price'><span>￥</span>{$goods_price}.<span>00</span></p>
                                </a>
                            </li>";
        }
        $html .= '?>';
        return $html;
    }
}