<?php
namespace app\admin\controller;

use app\admin\model\AdModel;
use app\admin\model\AdPositionModel;
use think\Db;

class Ad extends Base
{
    //*********************************************广告列表*********************************************//
    /**
     * 广告列表
     * @return mixed|\think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @author   [小码哥]
     */
    public function index()
    {
        if(request()->isPost()){
            $key           = input('key');
            $map           = [];
            $map['closed'] = 0;
            if($key){
                $map['title'] = ['like', "%" . $key . "%"];
            }
            $page  = input('page');
            $limit = input('limit');
            $count = Db('ad')->where($map)->count();//计算总页面
            $ad    = new AdModel();
            $lists = $ad->getAdAll($map, $page, $limit);
            return json(['code' => 0, 'data' => $lists, 'count' => $count, 'msg' => '广告列表']);
        }
        return $this->fetch();
    }

    /**
     * 添加广告
     *
     * @return mixed|\think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @author   [小码哥]
     */
    public function add_ad()
    {
        if(request()->isAjax()){
            $param               = input('post.');
            $param['closed']     = 0;
            $time                = explode(' ~ ', $param['time']);
            $param['start_date'] = strtotime($time[0]);
            $param['end_date']   = strtotime($time[1]);
            $ad   = new AdModel();
            $flag = $ad->insertAd($param);
            return json($flag);
        }
        $position = new AdPositionModel();
        $this->assign('position', $position->getAllPosition());
        return $this->fetch();
    }

    /**
     * 编辑广告
     *
     * @return mixed|\think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @author   [小码哥]
     */
    public function edit_ad()
    {
        $ad = new AdModel();
        if(request()->isPost()){
            $param = input('post.');
            $time                = explode('~', $param['time']);
            $param['start_date'] = strtotime($time[0]);
            $param['end_date']   = strtotime($time[1]);
            $flag  = $ad->editAd($param);
            return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
        }
        $id       = input('param.id');
        $position = new AdPositionModel();
        $this->assign('position', $position->getAllPosition());
        $item         = $ad->getOneAd($id);
        $item['time'] = "{$item['start_date']} ~ {$item['end_date']}";
        $this->assign('ad', $item);
        return $this->fetch();
    }

    /**
     * 删除广告
     *
     * @return \think\response\Json
     * @author   [小码哥]
     */
    public function del_ad()
    {
        $id   = input('param.id');
        $ad   = new AdModel();
        $flag = $ad->delAd($id);
        return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
    }

    /**
     * 广告状态
     *
     * @return \think\response\Json
     * @author   [小码哥]
     */
    public function ad_state()
    {
        $id     = input('param.id');
        $status = Db::name('ad')->where(array('id' => $id))->value('status');//判断当前状态情况
        if($status == 1){
            $flag = Db::name('ad')->where(array('id' => $id))->setField(['status' => 0]);
            return json(['code' => 1, 'data' => $flag['data'], 'msg' => '已禁止']);
        }else{
            $flag = Db::name('ad')->where(array('id' => $id))->setField(['status' => 1]);
            return json(['code' => 0, 'data' => $flag['data'], 'msg' => '已开启']);
        }
    }



    //*********************************************广告位*********************************************//

    /**
     * 广告位列表
     *
     * @return mixed|\think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @author   [小码哥]
     */
    public function index_position()
    {
        if(request()->isPost()){
            $ad    = new AdPositionModel();
            $page  = input('page');
            $limit = input('limit');
            $count = Db::name('ad_position')->count();//计算总页面
            $list  = $ad->getAll($page, $limit);
            return json(['code' => 0, 'data' => $list, 'count' => $count, 'msg' => '广告列表']);
        }
        return $this->fetch();
    }

    /**
     * 添加广告位
     *
     * @return mixed|\think\response\Json
     * @author   [小码哥]
     */
    public function add_position()
    {
        if(request()->isAjax()){

            $param = input('post.');
            $ad    = new AdPositionModel();
            $flag  = $ad->insertAdPosition($param);
            return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
        }
        return $this->fetch();
    }

    /**
     * 编辑广告位
     *
     * @return mixed|\think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @author   [小码哥]
     */
    public function edit_position()
    {
        $ad = new AdPositionModel();
        if(request()->isAjax()){
            $param = input('post.');
            $flag  = $ad->editAdPosition($param);
            return json($flag);
        }
        $id = input('param.id');
        $this->assign('ad', $ad->getOne($id));
        return $this->fetch();
    }

    /**
     * 删除广告位
     *
     * @return \think\response\Json
     * @author   [小码哥]
     */
    public function del_position()
    {
        $id   = input('param.id');
        $ad   = new AdPositionModel();
        $flag = $ad->delAdPosition($id);
        return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
    }

    /**
     * 广告位状态
     *
     * @return \think\response\Json
     * @author   [小码哥]
     */
    public function position_state()
    {
        $id     = input('param.id');
        $status = Db::name('ad_position')->where(array('id' => $id))->value('status');//判断当前状态情况
        if($status == 1){
            $flag = Db::name('ad_position')->where(array('id' => $id))->setField(['status' => 0]);
            return json(['code' => 1, 'data' => $flag['data'], 'msg' => '已禁止']);
        }else{
            $flag = Db::name('ad_position')->where(array('id' => $id))->setField(['status' => 1]);
            return json(['code' => 0, 'data' => $flag['data'], 'msg' => '已开启']);
        }
    }
}