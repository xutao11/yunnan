<?php
/**
 * Created by PhpStorm.
 * User: PHP
 * Date: 2018/1/10
 * Time: 10:52
 */
namespace app\admin\controller;
class Setting extends Base
{
    /**
     * 奖金设置
     *
     * @return mixed|\think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @author   [小码哥]
     */
    public function bonus()
    {
        $Setting = new \app\commen\model\Setting();
        $type    = input('type', 'jiangjing');
        if(request()->isPost()){
            $param = input('post.');
            unset($param['type']);
            $result = $Setting->editSetting($type, $param);
            return json($result);
        }
        $setting = $Setting->getSetting($type);
        return $this->fetch('bonus', ['setting' => $setting]);
    }

    /**
     * 提现设置
     *
     * @return mixed|\think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @author   [小码哥]
     */
    public function withdrawals()
    {
        $Setting = new \app\commen\model\Setting();
        $type    = input('type', 'tixian');
        if(request()->isPost()){
            $param = input('post.');
            unset($param['type']);
            $result = $Setting->editSetting($type, $param);
            return json($result);
        }
        $setting = $Setting->getSetting($type);
        return $this->fetch('withdrawals', ['setting' => $setting]);
    }

    /**
     * 账号前缀设置
     *
     * @return mixed|\think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @author   [小码哥]
     */
    public function account()
    {
        $Setting = new \app\commen\model\Setting();
        $type    = input('type', 'account');
        if(request()->isPost()){
            $param = input('post.');
            unset($param['type']);
            $message = $this->validate($param, [
                'name|前缀' => 'require|alpha|length:2'
            ], [
                'name.length' => '前缀只是为2位字母'
            ]);
            if(true !== $message){
                return json(['code' => 0, 'data' => null, 'msg' => $message]);
            }
            $result = $Setting->editSetting($type, $param);
            return json($result);
        }
        $setting = $Setting->getSetting($type);
        return $this->fetch('account', ['setting' => $setting]);
    }
}