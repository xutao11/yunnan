<?php
namespace app\admin\controller;

use app\admin\model\LogModel;
use think\Db;
use com\IpLocation;

class Log extends Base
{
    /**
     * 操作日志
     *
     * @return mixed|\think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @author   [小码哥]
     */
    public function operate_log()
    {
        if(request()->isPost()){
            $key = input('key', '');
            $map = [];
            if($key){
                $map['admin_id'] = $key;
            }
            $page  = input('page', 1);
            $limit = input('limit', 10);// 获取总条数
            $count = Db::name('log')->where($map)->count();//计算总页面
            $lists = Db::name('log')->where($map)->page($page, $limit)->order('add_time desc')->select();
            $Ip    = new IpLocation('UTFWry.dat'); // 实例化类 参数表示IP地址库文件
            foreach($lists as $k => $v){
                $lists[$k]['add_time'] = date('Y-m-d H:i:s', $v['add_time']);
                $lists[$k]['ipaddr']   = $Ip->getlocation($lists[$k]['ip']);
            }
            return json(['code' => 0, 'data' => $lists, 'count' => $count, 'msg' => '日志列表']);
        }
        $arr = Db::name("admin")->column("id,username"); //获取用户列表
        $this->assign("search_user", $arr);
        return $this->fetch();
    }

    /**
     * 删除日志
     * @return \think\response\Json
     * @author   [小码哥]
     */
    public function del_log()
    {
        $id   = input('id',0,'intval');
        $log  = new LogModel();
        $flag = $log->delLog($id);
        return json($flag);
    }
}