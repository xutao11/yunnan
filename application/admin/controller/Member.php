<?php
namespace app\admin\controller;

use app\admin\model\CapitalDetails;
use app\admin\model\MemberModel;
use app\admin\model\MemberGroupModel;
use app\admin\model\Shop;
use app\admin\model\TransactionRecord;
use think\Db;

class Member extends Base
{
    //*********************************************会员组*********************************************//
    /**
     * [group 会员组]
     *
     * @author
     */
    public function group()
    {
        if(request()->isPost()){
            $key = input('key','');
            $map = [];
            if($key){
                $map['group_name'] = ['like', "%" . $key . "%"];
            }
            $group = new MemberGroupModel();
            $page  = input('page', 1);
            $limit = input('limit', 10);
            $count = $group->getAllCount($map);
            $lists = $group->getAll($map, $page, $limit);
            return json(['code' => 0, 'data' => $lists, 'count' => $count, 'msg' => '角色列表']);
        }
        return $this->fetch();
    }

    /**
     * [add_group 添加会员组]
     *
     * @author
     */
    public function add_group()
    {
        if(request()->isAjax()){
            $param = input('post.');
            $group = new MemberGroupModel();
            $flag  = $group->insertGroup($param);
            return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
        }
        return $this->fetch();
    }

    /**
     * [edit_group 编辑会员组]
     *
     * @author
     */
    public function edit_group()
    {
        $group = new MemberGroupModel();
        if(request()->isPost()){
            $param = input('post.');
            $flag  = $group->editGroup($param);
            return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
        }
        $id = input('param.id');
        $this->assign('group', $group->getOne($id));
        return $this->fetch();
    }

    /**
     * [del_group 删除会员组]
     *
     * @author
     */
    public function del_group()
    {
        $id    = input('param.id');
        $group = new MemberGroupModel();
        $flag  = $group->delGroup($id);
        return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
    }

    /**
     * [group_status 会员组状态]
     *
     * @author
     */
    public function group_status()
    {
        $id     = input('param.id');
        $status = Db::name('member_group')->where(array('id' => $id))->value('status');//判断当前状态情况
        if($status == 1){
            $flag = Db::name('member_group')->where(array('id' => $id))->setField(['status' => 0]);
            return json(['code' => 1, 'data' => $flag['data'], 'msg' => '已关闭']);
        }else{
            $flag = Db::name('member_group')->where(array('id' => $id))->setField(['status' => 1]);
            return json(['code' => 0, 'data' => $flag['data'], 'msg' => '已开启']);
        }
    }


    //*********************************************会员列表*********************************************//

    /**
     * 会员列表
     *
     * @return mixed|\think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @author   [小码哥]
     */
    public function index()
    {
        if(request()->isPost()){
            $where = [];
            $key   = input('key', '');
            if($key){
                $where['account|mobile|nickname'] = ['LIKE', "%{$key}%"];
            }
            $member  = new MemberModel();
            $page    = input('page', 1);
            $limit   = input('limit', 10);
            $field   = input('field', '');
            $order   = input('order', '');
            $orderBy = ['status' => 'ASC', 'group_id' => 'DESC', 'closed' => 'DESC', 'create_time' => 'DESC'];
            if($field && $order){
                $orderBy = [$field => $order];
            }
            $count = $member->getAllCount($where);//计算总页面
            $lists = $member->getMemberByWhere($where, $page, $limit, $orderBy);
            return json(['code' => 0, 'data' => $lists, 'count' => $count, 'msg' => '会员列表']);
        }
        //获取商户的列表
        $shop_list = Shop::where('user_id',null)->select();
        $this->assign('shop_list',$shop_list);
        return $this->fetch();
    }

    /**
     * 添加会员
     *
     * @return mixed|\think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @author   [小码哥]
     */
    public function add_member()
    {
        if(request()->isAjax()){
            $param             = input('post.');
            $param['password'] = md5(md5($param['password']) . config('auth_key'));
            $param['security'] = md5(md5($param['security']) . config('auth_key'));
            $member            = new MemberModel();
            $flag              = $member->insertMember($param);
            return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
        }
        $group = new MemberGroupModel();
        $this->assign('group', $group->getGroup());
        return $this->fetch();
    }

    /**
     * 编辑会员
     *
     * @return mixed|\think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @author   [小码哥]
     */
    public function edit_member()
    {
        $member = new MemberModel();
        if(request()->isAjax()){
            $param = input('post.');
            if(empty($param['password'])){
                unset($param['password']);
            }else{
                $param['password'] = md5(md5($param['password']) . config('auth_key'));
            }
            if(empty($param['security'])){
                unset($param['security']);
            }else{
                $param['security'] = md5(md5($param['security']) . config('auth_key'));
            }
            $flag = $member->editMember($param);
            return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
        }
        $id    = input('param.id');
        $group = new MemberGroupModel();
        $this->assign([
            'member' => $member->getOneMember($id),
            'group'  => $group->getGroup()
        ]);
        return $this->fetch();
    }

    /**
     * 删除会员
     *
     * @author
     */
    public function del_member()
    {
        $id     = input('param.id');
        $member = new MemberModel();
        $flag   = $member->delMember($id);
        return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
    }

    /**
     * 会员状态
     *
     * @author
     */
    public function member_status()
    {
        $id     = input('id');
        $status = Db::name('member')->where('id', $id)->value('closed');//判断当前状态情况
        if($status == 1){
            Db::name('member')->where('id', $id)->setField(['closed' => 0]);
            return json(['code' => 200, 'data' => 0, 'msg' => '已开启']);
        }else{
            Db::name('member')->where('id', $id)->setField(['closed' => 1]);
            return json(['code' => 200, 'data' => 1, 'msg' => '已冻结']);
        }
    }

    /**
     * 加减积分
     *
     * @return \think\response\Json
     * @throws \Exception
     * @throws \think\Exception
     * @author   [小码哥]
     */
    public function recharge()
    {
        $param  = input('post.');
        $Member = new MemberModel();
        if($param['type'] == 'inc'){
            $result = $Member->where('account', $param['account'])->setInc($param['integral_type'], $param['integral']);
            $msg    = '充值';
            if($result){
                $balance = $Member->where('account', $param['account'])->value($param['integral_type']);
                return json(['code' => 200, 'data' => [$param['integral_type'] => $balance], 'msg' => "{$msg}成功"]);
            }
        }else{
            $balance = $Member->where('account', $param['account'])->value($param['integral_type']);
            if($balance < $param['integral']){
                return json(['code' => 0, 'data' => null, 'msg' => "余额不足、扣除失败"]);
            }
            $result = $Member->where('account', $param['account'])->setDec($param['integral_type'], $param['integral']);
            $msg    = '扣除';
            if($result){
                $balance = $Member->where('account', $param['account'])->value($param['integral_type']);
                return json(['code' => 200, 'data' => [$param['integral_type'] => $balance], 'msg' => "{$msg}成功"]);
            }
        }
        return json(['code' => 0, 'data' => null, 'msg' => "{$msg}失败"]);
    }
    //绑定商户
    public function binding_shop(){
        if(request()->isAjax()){
            $shop_id = input('post.shop_id');
            $user_id = session('uid');
            $re = Shop::where('id',$shop_id)->update([
                'user_id'=>$user_id
            ]);
            $shop = Shop::find($shop_id);
            TransactionRecord::where('shop_number',$shop->shop_number)->update([
                'user_id'=>$user_id
            ]);
            if($re){
                return json(['code' => 200, 'data' => $re, 'msg' => "绑定成功"]);
            }else{
                return json(['code' => 0, 'data' => $re, 'msg' => "绑定失败"]);
            }
        }else{
            return json(['code' => 0, 'data' => '', 'msg' => "非法请求"]);
        }
    }
    public function test(){
        echo '<pre>';
        TransactionRecord::every_date(1);
    }

}