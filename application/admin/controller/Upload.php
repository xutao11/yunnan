<?php
namespace app\admin\controller;

use think\Controller;
use think\Exception;
use think\File;
use think\Request;

class Upload extends Base
{
    //图片上传
    public function upload()
    {
        $file = request()->file('file');
        $info = $file->move(ROOT_PATH . 'static' . DS . 'uploads/images');
        if($info){
            echo $info->getSaveName();
        }else{
            echo $file->getError();
        }
    }

    //会员头像上传
    public function uploadface()
    {
        $file = request()->file('file');
        $info = $file->move(ROOT_PATH . 'static' . DS . 'uploads/face');
        if($info){
            echo $info->getSaveName();
        }else{
            echo $file->getError();
        }
    }

    public function uploads()
    {
        $path = input('path', 'images');
        $file = request()->file('file');
        $info = $file->move(ROOT_PATH . 'static' . DS . 'uploads' . DS . $path);
        if($info){
            return json(['code' => 0, 'data' => str_replace('\\', '/', $info->getSaveName()), 'msg' => '上传成功']);
        }
        return json(['code' => 1, 'data' => [], 'msg' => $info->getError()]);
    }

    public function upload_base64()
    {
        try{
            $path   = input('path', 'base64');
            $base64 = htmlspecialchars(input('file'));
            if(preg_match('/^(data:\s*image\/(\w+);base64,)/', $base64, $result)){
                // 文件后缀
                $type = $result[2];
                // 新的文件名称
                $filename = date('Ymd', time()) . DS . md5(time()) . '.' . $type;
                $filename = str_replace('\\', '/', $filename);
                // 文件路径
                if(!file_exists(ROOT_PATH . 'static' . DS . 'uploads' . DS . $path)){
                    mkdir(ROOT_PATH . 'static' . DS . 'uploads' . DS . $path, 0777);
                }
                if(!file_exists(ROOT_PATH . 'static' . DS . 'uploads' . DS . $path . DS . date('Ymd', time()))){
                    mkdir(ROOT_PATH . 'static' . DS . 'uploads' . DS . $path . DS . date('Ymd', time()), 0777);
                }
                $new_file = ROOT_PATH . 'static' . DS . 'uploads' . DS . $path . DS . $filename;
                // 反斜杠转义
                $new_file = str_replace('\\', '/', $new_file);
                $base64   = base64_decode(str_replace($result[1], '', $base64));
                //写入文件
                if(@file_put_contents($new_file, $base64)){
                    return json(['code' => 0, 'data' => $filename, 'msg' => '上传成功']);
                }
                return json(['code' => 1, 'data' => null, 'msg' => '上传失败']);
            }
            return json(['code' => 1, 'data' => null, 'msg' => '上传错误']);
        }catch(Exception $e){
            return json(['code' => 1, 'data' => null, 'msg' => $e->getMessage()]);
        }
    }

    public function uploadExcel()
    {
        $file = request()->file('file');
        $info = $file->validate(['ext'=>'xls,xlsx'])->rule('uniqid')->move(ROOT_PATH . 'temp');
        if (!$info) {
            return json([
                'code' => 0,
                'data' => null,
                'msg' => $file->getError()
            ]);
        }

        return json([
            'code' => 200,
            'data' => str_replace('\\', '/', ROOT_PATH . 'temp/' . $info->getSaveName()),
            'msg' => '上传成功'
        ]);
    }
}