<?php
namespace app\admin\controller;

use app\admin\model\Admin;
use app\admin\model\AuthGroup;
use app\commen\model\WithdrawalsLog;
use think\Db;

class User extends Base
{
    /**
     * 用户列表
     * @return mixed|\think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @author   [小码哥]
     */
    public function index()
    {
        if(request()->isPost()){
            $key = input('key');
            $map = [];
            if($key){
                $map['username'] = ['like', "%" . $key . "%"];
            }
            $page  = input('page', 1);
            $limit = input('limit', 10);// 获取总条数
            $count = Db::name('admin')->where($map)->count();//计算总页面
            $user  = new Admin();
            $lists = $user->getUsersByWhere($map, $page, $limit);
            foreach($lists as $k => $v){
                $lists[$k]['last_login_time'] = date('Y-m-d H:i:s', $v['last_login_time']);
            }
            return json(['code' => 0, 'data' => $lists, 'count' => $count, 'msg' => '管理员列表']);
        }
        return $this->fetch();
    }

    /**
     * [userAdd 添加用户]
     *
     * @return [type] [description]
     * @author 
     */
    public function userAdd()
    {
        if(request()->isAjax()){

            $param             = input('post.');
            $param['password'] = md5(md5($param['password']) . config('auth_key'));
            $user              = new Admin();
            $flag              = $user->insertUser($param);
            $accdata           = array(
                'uid'      => $user['id'],
                'group_id' => $param['groupid'],
            );
            $group_access      = Db::name('auth_group_access')->insert($accdata);
            return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
        }
        $role = new AuthGroup();
        $this->assign('role', $role->getRole());
        return $this->fetch();
    }

    /**
     * 编辑用户
     *
     * @return mixed|\think\response\Json
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     * @author   [小码哥]
     */
    public function userEdit()
    {
        $user = new Admin();
        if(request()->isAjax()){

            $param = input('post.');
            if(empty($param['password'])){
                unset($param['password']);
            }else{
                $param['password'] = md5(md5($param['password']) . config('auth_key'));
            }
            $flag         = $user->editUser($param);
            $group_access = Db::name('auth_group_access')->where('uid', $user['id'])->update(['group_id' => $param['groupid']]);
            return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
        }
        $id   = input('param.id');
        $role = new AuthGroup();
        $this->assign([
            'user' => $user->getOneUser($id),
            'role' => $role->getRole()
        ]);
        return $this->fetch();
    }

    /**
     * [UserDel 删除用户]
     *
     * @return [type] [description]
     * @author 
     */
    public function UserDel()
    {
        $id   = input('param.id');
        $role = new Admin();
        $flag = $role->delUser($id);
        return json(['code' => $flag['code'], 'data' => $flag['data'], 'msg' => $flag['msg']]);
    }

    /**
     * [user_state 用户状态]
     *
     * @return [type] [description]
     * @author 
     */
    public function user_state()
    {
        $id     = input('param.id');
        $status = Db::name('admin')->where('id', $id)->value('status');//判断当前状态情况
        if($status == 1){
            $flag = Db::name('admin')->where('id', $id)->setField(['status' => 0]);
            return json(['code' => 1, 'data' => $flag['data'], 'msg' => '已禁止']);
        }else{
            $flag = Db::name('admin')->where('id', $id)->setField(['status' => 1]);
            return json(['code' => 0, 'data' => $flag['data'], 'msg' => '已开启']);
        }
    }
}