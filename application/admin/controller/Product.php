<?php
/**
 * Created by PhpStorm.
 * User: PHP
 * Date: 2018/1/26
 * Time: 16:08
 */
namespace app\admin\controller;

use app\admin\model\Brand;
use app\admin\model\Goods;
use app\admin\model\GoodsCate;
use think\Db;

class Product extends Base
{
    // -------------------------------------------------- 品牌 start --------------------------------------------------
    /**
     * 列表
     *
     * @return mixed|\think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @author   [小码哥]
     */
    public function brand()
    {
        if(request()->isPost()){
            $where = [];
            $page  = input('page', 1);
            $limit = input('limit', 10);
            $Brand = new Brand();
            $count = $Brand->where($where)->count();
            $data  = $Brand->getBrand($where, $page, $limit);
            return json(['code' => 0, 'data' => $data, 'count' => $count, 'msg' => '品牌列表']);
        }
        return $this->fetch();
    }

    /**
     * 添加
     *
     * @return mixed
     * @author   [小码哥]
     */
    public function brand_add()
    {
        if(request()->isPost()){
            $param   = input('post.');
            $message = $this->validate($param, [
                'brand_name|品牌名称'   => 'require',
                'brand_logo|品牌LOGO' => 'require',
                'brand_url|品牌链接'    => 'url',
                'brand_sort|排序'     => 'number',
            ]);
            if(true !== $message){
                return json(['code' => 0, 'data' => null, 'msg' => $message]);
            }
            $Brand  = new Brand();
            $result = $Brand->brandAdd($param);
            return json($result);
        }
        return $this->fetch();
    }

    /**
     * 编辑
     *
     * @return mixed|\think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @author   [小码哥]
     */
    public function brand_edit()
    {
        $Brand = new Brand();
        if(request()->isPost()){
            $param  = input('post.');
            $result = $Brand->brandEdit($param);
            return json($result);
        }
        $brand_id = input('brand_id', 0);
        $item     = $Brand->getItem($brand_id);
        return $this->fetch('brand_edit', ['item' => $item]);
    }

    /**
     * 删除
     *
     * @return \think\response\Json
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     * @author   [小码哥]
     */
    public function brand_del()
    {
        $brand_id = input('brand_id', 0);
        $result   = Db::name('Brand')->where('brand_id', $brand_id)->delete();
        if($result){
            return json(['code' => 200, 'data' => null, 'msg' => '删除成功']);
        }
        return json(['code' => 0, 'data' => null, 'msg' => '删除失败']);
    }

    /**
     * 状态
     *
     * @return \think\response\Json
     * @author   [小码哥]
     */
    public function brand_status()
    {
        $brand_id  = input('brand_id', 0);
        $brand_tui = Db::name('Brand')->where('brand_id', $brand_id)->value('brand_tui');
        $brand_tui = ($brand_tui == 1) ? 0 : 1;
        $result    = Db::name('Brand')->where('brand_id', $brand_id)->setField('brand_tui', $brand_tui);
        if($result){
            return json(['code' => 200, 'data' => null, 'msg' => '更改成功']);
        }
        return json(['code' => 0, 'data' => null, 'msg' => '更改失败']);
    }
    // -------------------------------------------------- 品牌 end --------------------------------------------------
    //-----
    // -------------------------------------------------- 产品分类 start --------------------------------------------------
    /**
     * 列表
     *
     * @return mixed|\think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @author   [小码哥]
     */
    public function product_cate()
    {
        if(request()->isPost()){
            $where     = [];
            $page      = input('page', 1);
            $limit     = input('limit', 10);
            $GoodsCate = new GoodsCate();
            $count     = $GoodsCate->where($where)->count();
            $data      = $GoodsCate->getLists($where, $page, $limit);
            return json(['code' => 0, 'data' => $data, 'count' => $count, 'msg' => '产品分类列表']);
        }
        return $this->fetch();
    }

    /**
     * 添加
     *
     * @return mixed|\think\response\Json
     * @author   [小码哥]
     */
    public function product_cate_add()
    {
        if(request()->isPost()){
            $param     = input('post.');
            $GoodsCate = new GoodsCate();
            $result    = $GoodsCate->productCateAdd($param);
            return json($result);
        }
        return $this->fetch();
    }

    /**
     * 编辑
     *
     * @return mixed|\think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @author   [小码哥]
     */
    public function product_cate_edit()
    {
        $GoodsCate = new GoodsCate();
        if(request()->isPost()){
            $param    = input('post.');
            $response = $GoodsCate->productCateEdit($param);
            return json($response);
        }
        $product_cate_id = input('product_cate_id', 0);
        $item            = $GoodsCate->getProductCateItem($product_cate_id);
        return $this->fetch('product_cate_edit', ['item' => $item]);
    }

    /**
     * 删除
     *
     * @return \think\response\Json
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     * @author   [小码哥]
     */
    public function product_cate_del()
    {
        $product_cate_id = input('product_cate_id', 0);
        $response        = Db::name('GoodsCate')->where('product_cate_id', $product_cate_id)->delete();
        if($response){
            return json(['code' => 200, 'data' => null, 'msg' => '删除成功']);
        }
        return json(['code' => 0, 'data' => null, 'msg' => '删除失败']);
    }

    /**
     * 状态
     *
     * @return \think\response\Json
     * @author   [小码哥]
     */
    public function product_cate_status()
    {
        $product_cate_id     = input('product_cate_id', 0);
        $product_cate_status = Db::name('GoodsCate')->where('product_cate_id', $product_cate_id)->value('product_cate_status');
        $product_cate_status = ($product_cate_status == 1) ? 0 : 1;
        $result              = Db::name('GoodsCate')->where('product_cate_id', $product_cate_id)->setField('product_cate_status', $product_cate_status);
        if($result){
            return json(['code' => 200, 'data' => null, 'msg' => '更改成功']);
        }
        return json(['code' => 0, 'data' => null, 'msg' => '更改失败']);
    }

    /**
     * 推荐
     *
     * @return \think\response\Json
     * @author   [小码哥]
     */
    public function product_cate_tui()
    {
        $product_cate_id  = input('product_cate_id', 0);
        $product_cate_tui = Db::name('GoodsCate')->where('product_cate_id', $product_cate_id)->value('product_cate_tui');
        $product_cate_tui = ($product_cate_tui == 1) ? 0 : 1;
        $result           = Db::name('GoodsCate')->where('product_cate_id', $product_cate_id)->setField('product_cate_tui', $product_cate_tui);
        if($result){
            return json(['code' => 200, 'data' => null, 'msg' => '更改成功']);
        }
        return json(['code' => 0, 'data' => null, 'msg' => '更改失败']);
    }
    // -------------------------------------------------- 产品分类 end --------------------------------------------------
    //-----
    // -------------------------------------------------- 产品列表 start --------------------------------------------------
    public function product()
    {
        if(request()->isPost()){
            $page  = input('page', 1);
            $limit = input('limit', 10);
            $where = [];
            $Goods = new Goods();
            $count = $Goods->where($where)->count();
            $data  = $Goods->getGoodsList($where, $page, $limit);
            return json(['code' => 0, 'data' => $data, 'count' => $count, 'msg' => '商品列表']);
        }
        $this->assign('demo_time', time());
        return $this->fetch();
    }

    /**
     * 添加商品
     *
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @author   [小码哥]
     */
    public function product_add()
    {
        if(request()->isPost()){
            $param = input('post.');
            if(empty($param['goods_no'])){
                $param['goods_no'] = 'MY_' . mt_rand(100000, 999999);
            }
            $Goods    = new Goods();
            $response = $Goods->goodsAdd($param);
            return json($response);
        }
        $GoodsCate = new GoodsCate();
        $Brand     = new Brand();
        $cate      = $GoodsCate->getLists(['product_cate_status' => 1], 1, 100);
        return $this->fetch('product_add', ['cate' => $cate, 'brand' => $Brand->getBrand([], 1, 100)]);
    }

    public function product_edit()
    {

    }

    public function product_del()
    {

    }

    public function product_status()
    {
        $param  = input('post.');
        $Goods  = new Goods();
        $status = $Goods->where('goods_id', $param['goods_id'])->value($param['field']);
        $ch     = $status == 1 ? 0 : 1;
        $Goods->where('goods_id', $param['goods_id'])->setField($param['field'], $ch);
        return json(['code' => 200, 'data' => null, 'msg' => '成功']);
    }
    // -------------------------------------------------- 产品列表 end --------------------------------------------------

    /**
     * 店铺设置
     *
     * @author   [小码哥]
     */
    public function setting()
    {
        if(request()->isPost()){
            $param = input('post.');
            $type  = input('type', 'wuliu');
            unset($param['type']);
            switch(strtolower($type)){
                case 'shop':
                case 'shop_tixian':
                    get_or_set_shop('set', $type, $param);
                    break;
                default:
                    set_kuai_di($param);
                    break;
            }
            return json(['code' => 200, 'data' => null, 'msg' => '设置成功']);
        }
        $code   = get_kuai_di();
        $shop   = get_or_set_shop('get', 'shop');
        $tixian = get_or_set_shop('get', 'shop_tixian');
        return $this->fetch('setting', ['code' => $code, 'shop' => $shop, 'tixian' => $tixian]);
    }
}