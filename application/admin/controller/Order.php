<?php
namespace app\admin\controller;
/**
 * 订单
 * Class Order
 *
 * @package app\admin\controller
 */
class Order extends Base
{
    /**
     * 订单列表
     *
     * @return mixed|\think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @author   [小码哥]
     */
    public function index()
    {
        if(request()->isPost()){
            $where = [];
            $page  = input('page', 1);
            $limit = input('limit', 10);
            $Order = new \app\admin\model\Order();
            $count = $Order->where($where)->count();
            $data  = $Order->getOrderLists($where, $page, $limit);
            return json(['code' => 0, 'data' => $data, 'count' => $count, 'msg' => '订单列表']);
        }
        return $this->fetch();
    }

    /**
     * 编辑订单
     *
     * @author   [小码哥]
     */
    public function index_edit()
    {

    }

    /**
     * 删除订单
     *
     * @author   [小码哥]
     */
    public function index_del()
    {

    }

    /**
     * 订单发货
     *
     * @author   [小码哥]
     */
    public function deliver()
    {

    }
}