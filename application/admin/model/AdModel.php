<?php
namespace app\admin\model;

use think\Model;
use think\Db;

class AdModel extends Model
{
    protected $name = 'ad';

    /**
     * 根据条件获取列表信息
     *
     * @param $map
     * @param $Nowpage
     * @param $limits
     *
     * @return false|\PDOStatement|string|\think\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @author   [小码哥]
     */
    public function getAdAll($map, $Nowpage, $limits)
    {
        return $this
            ->join('think_ad_position', 'think_ad.ad_position_id = think_ad_position.id')
            ->where($map)
            ->page($Nowpage, $limits)
            ->field('think_ad.*,name')
            ->order('orderby desc')
            ->select();
    }

    /**
     * 插入信息
     *
     * @param $param
     *
     * @return array
     * @author   [小码哥]
     */
    public function insertAd($param)
    {
        try{
            $result = $this->validate('AdValidate')->allowField(true)->save($param);
            if(false === $result){
                return ['code' => -1, 'data' => '', 'msg' => $this->getError()];
            }else{
                return ['code' => 1, 'data' => '', 'msg' => '添加广告成功'];
            }
        }catch(PDOException $e){
            return ['code' => -2, 'data' => '', 'msg' => $e->getMessage()];
        }
    }

    /**
     * 编辑信息
     *
     * @param $param
     *
     * @return array
     * @author   [小码哥]
     */
    public function editAd($param)
    {
        try{

            $result = $this->validate('AdValidate')->allowField(true)->save($param, ['id' => $param['id']]);
            if(false === $result){
                return ['code' => 0, 'data' => '', 'msg' => $this->getError()];
            }else{
                return ['code' => 1, 'data' => '', 'msg' => '编辑广告成功'];
            }
        }catch(PDOException $e){
            return ['code' => 0, 'data' => '', 'msg' => $e->getMessage()];
        }
    }

    /**
     * 根据id获取一条信息
     *
     * @param $id
     *
     * @return array|false|\PDOStatement|string|Model
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @author   [小码哥]
     */
    public function getOneAd($id)
    {
        return $this->where('id', $id)->find();
    }

    /**
     * 删除信息
     *
     * @param $id
     *
     * @return array
     * @author   [小码哥]
     */
    public function delAd($id)
    {
        try{
            $map['closed'] = 1;
            $this->save($map, ['id' => $id]);
            return ['code' => 1, 'data' => '', 'msg' => '删除广告成功'];
        }catch(PDOException $e){
            return ['code' => 0, 'data' => '', 'msg' => $e->getMessage()];
        }
    }

    public function getStartDateAttr($value)
    {
        return date('Y-m-d H:i:s', $value);
    }

    public function getEndDateAttr($value)
    {
        return date('Y-m-d H:i:s', $value);
    }

    public function getClosedAttr($value, $data)
    {
        $msg = '';
        if($data['start_date'] > time()){
            $msg .= '未开始';
        }elseif($data['start_date'] <= time() && $data['end_date'] > time()){
            $msg .= '<font color="#5FB878">投放中</font>';
        }else{
            $msg .= '<font color="red">已结束</font>';
        }
        return $msg;
    }
}