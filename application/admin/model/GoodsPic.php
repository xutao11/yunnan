<?php
/**
 * Created by PhpStorm.
 * User: PHP
 * Date: 2018/2/6
 * Time: 10:41
 */
namespace app\admin\model;

use think\exception\PDOException;
use think\Model;

class GoodsPic extends Model
{
    /**
     * 添加商品缩略图
     * @param array $param
     *
     * @return array
     * @throws \Exception
     * @author   [小码哥]
     */
    public function picAdd($param = [])
    {
        try{
            $result = $this->allowField(true)->saveAll($param);
            if($result){
                return ['code' => 200, 'data' => null, 'msg' => '添加成功'];
            }
            return ['code' => 0, 'data' => null, 'msg' => '添加失败'];
        }catch(PDOException $e){
            return ['code' => 0, 'data' => null, 'msg' => $e->getMessage()];
        }
    }
}