<?php
/**
 * Created by PhpStorm.
 * User: PHP
 * Date: 2018/2/3
 * Time: 14:21
 */
namespace app\admin\model;

use think\Db;
use think\Model;
/**
 * 交易记录
 * 徐涛
 *2018.3.5
 */

class TransactionRecord extends Model
{
    //手续费总和的利润
    //获取当前用户的手续费综合
    //$percentage 总和的分润比例
    public static function shouxu_money($user_id,$percentage){
        $re = self::where('user_id',$user_id)->select();
        //综合
        $num = 0;
        if($re){
            foreach($re as $v){
                $num+=(int)$v['service_money'];
            }
        }
        $lirun =  number_format($num*$percentage, 2); //10.46
        return $lirun;
    }
    //每日返还
    public static function every_date ($user_id,$percentage = 0.1){
        $re = self::where('user_id',$user_id)->field('trading_date')->select();
        $dates = [];
        foreach($re as $value){
            if(!in_array($value['trading_date'],$dates)){
                $dates[] = $value['trading_date'];
            }
        }
        //查询每天的和
        $date_num = 0;
        //返利为
        $date_money = 0;
        foreach($dates as $k=>$v){
            $re = self::where('user_id',$user_id)->where('trading_date',$v)->select();
            if($re){
                foreach($re as $k1=>$v1){
                    $date_num+=$v1['service_money'];
                }
            }
            //当一天的手续费》2000时计算返利
            if($date_num>=2000){
                $date_money =number_format($date_num*$percentage,2);
            }
            $arr[$v]['date_num'] = $date_num;
            $arr[$v]['date_money'] = $date_money;
        }
        print_r($arr);
//        $re = Db::name('transaction_record')->where('user_id',$user_id)->group('shop_number')->select();
//        var_dump($re);
    }

}