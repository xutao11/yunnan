<?php
namespace app\admin\model;

use app\commen\model\Group;
use think\exception\PDOException;
use think\Model;
use think\Db;

class MemberModel extends Model
{
    protected $name               = 'member';
    protected $autoWriteTimestamp = true;   // 开启自动写入时间戳

    /**
     * 根据搜索条件获取用户列表信息
     * @param       $map
     * @param       $Nowpage
     * @param       $limits
     * @param array $order
     *
     * @return false|\PDOStatement|string|\think\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @author   [小码哥]
     */
    public function getMemberByWhere($map, $Nowpage, $limits,$order=[])
    {
        $list =  $this
            ->alias('a')
            ->join(['think_member_group' => 'b'], 'a.group_id = b.id')
            ->where($map)
            ->field('a.*,b.group_name')
            ->page($Nowpage, $limits)
            ->order($order)
            ->select();
        foreach($list as $key => $value){
            $value['closed_name'] = $value['closed'];
            $list[$key] = $value;
            //查询该用户下的交易记录的手续费总额和
            $shouxu_money = shouxu_money($value['id']);
            $list[$key]['shouxu_money'] = $shouxu_money;
        }
        return $list;
    }

    /**
     * 根据搜索条件获取所有的用户数量
     *
     * @param $map
     *
     * @return int|string
     * @author   [小码哥]
     */
    public function getAllCount($map)
    {
        return $this->where($map)->count();
    }

    /**
     * 插入信息
     *
     * @param $param
     *
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @author   [小码哥]
     */
    public function insertMember($param)
    {
        try{
            $result = $this->validate('MemberValidate')->allowField(true)->save($param);
            if(false === $result){
                return ['code' => -1, 'data' => '', 'msg' => $this->getError()];
            }else{
                $Group = new Group();
                $group = $Group->select();
                if(!$group){
                    $Group->save(['account' => $param['account'], 'group_id' => mt_rand(1000000, 9999999), 'level' => 1, 'pid' => 0]);
                }
                return ['code' => 1, 'data' => '', 'msg' => '添加成功'];
            }
        }catch(PDOException $e){
            return ['code' => -2, 'data' => '', 'msg' => $e->getMessage()];
        }
    }

    /**
     * 编辑信息
     *
     * @param $param
     *
     * @return array
     * @author   [小码哥]
     */
    public function editMember($param)
    {
        try{
            $result = $this->validate('MemberValidate')->allowField(true)->save($param, ['id' => $param['id']]);
            if(false === $result){
                return ['code' => 0, 'data' => '', 'msg' => $this->getError()];
            }else{
                return ['code' => 1, 'data' => '', 'msg' => '编辑成功'];
            }
        }catch(PDOException $e){
            return ['code' => 0, 'data' => '', 'msg' => $e->getMessage()];
        }
    }

    /**
     * 根据管理员id获取角色信息
     *
     * @param $id
     *
     * @return array|false|\PDOStatement|string|Model
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @author   [小码哥]
     */
    public function getOneMember($id)
    {
        return $this->where('id', $id)->find();
    }

    /**
     * 真删除管理员
     *
     * @param $id
     *
     * @return array
     * @throws \think\Exception
     * @author   [小码哥]
     */
    public function delUser($id)
    {
        try{

            $this->where('id', $id)->delete();
            Db::name('auth_group_access')->where('uid', $id)->delete();
            return ['code' => 1, 'data' => '', 'msg' => '删除成功'];
        }catch(PDOException $e){
            return ['code' => 0, 'data' => '', 'msg' => $e->getMessage()];
        }
    }

    /**
     * 软删除账号
     *
     * @param $id
     *
     * @return array
     * @author   [小码哥]
     */
    public function delMember($id)
    {
        try{
            $map['closed'] = 1;
            $this->save($map, ['id' => $id]);
            return ['code' => 1, 'data' => '', 'msg' => '删除成功'];
        }catch(PDOException $e){
            return ['code' => 0, 'data' => '', 'msg' => $e->getMessage()];
        }
    }

}