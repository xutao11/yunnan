<?php
/**
 * Created by PhpStorm.
 * User: PHP
 * Date: 2018/1/29
 * Time: 11:10
 */
namespace app\admin\model;

use think\exception\PDOException;
use think\Model;

class GoodsCate extends Model
{
    protected $autoWriteTimestamp = true;

    /**
     * 获取分类
     *
     * @param array $where
     * @param int   $page
     * @param int   $limit
     *
     * @return false|\PDOStatement|string|\think\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @author   [小码哥]
     */
    public function getLists($where = [], $page = 1, $limit = 10)
    {
        return $this->where($where)->page($page, $limit)->select();
    }

    /**
     * 添加分类
     *
     * @param $param
     *
     * @return array
     * @author   [小码哥]
     */
    public function productCateAdd($param)
    {
        try{
            $result = $this->allowField(true)->save($param);
            if($result){
                return ['code' => 200, 'data' => null, 'msg' => '添加成功'];
            }
            return ['code' => 0, 'data' => null, 'msg' => '添加失败'];
        }catch(PDOException $e){
            return ['code' => 0, 'data' => null, 'msg' => $e->getMessage()];
        }
    }

    /**
     * 编辑分类
     *
     * @param $param
     *
     * @return array
     * @author   [小码哥]
     */
    public function productCateEdit($param)
    {
        try{
            $result = $this->allowField(true)->save($param, ['product_cate_id' => $param['product_cate_id']]);
            if($result){
                return ['code' => 200, 'data' => null, 'msg' => '编辑成功'];
            }
            return ['code' => 0, 'data' => null, 'msg' => '编辑添加失败'];
        }catch(PDOException $e){
            return ['code' => 0, 'data' => null, 'msg' => $e->getMessage()];
        }
    }

    /**
     * 获取单条记录
     *
     * @param $product_cate_id
     *
     * @return array|false|\PDOStatement|string|Model
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @author   [小码哥]
     */
    public function getProductCateItem($product_cate_id)
    {
        return $this->where('product_cate_id', $product_cate_id)->find();
    }
}