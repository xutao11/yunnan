<?php
/**
 * Created by PhpStorm.
 * User: PHP
 * Date: 2018/2/6
 * Time: 10:40
 */
namespace app\admin\model;

use think\exception\PDOException;
use think\Model;

class Goods extends Model
{
    protected $autoWriteTimestamp = true;

    /**
     * 获取商品列表
     *
     * @param array $where
     * @param int   $page
     * @param int   $limit
     * @param array $order
     *
     * @return mixed
     * @author   [小码哥]
     */
    public function getGoodsList($where = [], $page = 1, $limit = 10, $order = ['create_time' => 'DESC'])
    {
        return $this->where($where)->page($page, $limit)->order($order)->select();
    }

    /**
     * 添加商品
     *
     * @param $param
     *
     * @return array
     * @author   [小码哥]
     */
    public function goodsAdd($param)
    {
        try{
            $result   = $this->allowField(true)->save($param);
            $goods_id = $this->getLastInsID();
            if($result){
                if(isset($param['pic']) && is_array($param['pic']) && count($param['pic']) > 0){
                    $pic = [];
                    foreach($param['pic'] as $item){
                        $li    = [
                            'goods_id' => $goods_id,
                            'path'     => $item
                        ];
                        $pic[] = $li;
                    }
                    $GoodsPic = new GoodsPic();
                    $GoodsPic->picAdd($pic);
                }
                return ['code' => 200, 'data' => $pic, 'msg' => '添加成功'];
            }
            return ['code' => 0, 'data' => null, 'msg' => '添加失败'];
        }catch(PDOException $e){
            return ['code' => 0, 'data' => null, 'msg' => $e->getMessage()];
        }
    }
}