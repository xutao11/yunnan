<?php
namespace app\admin\model;

use think\Model;
use think\Db;

class AdPositionModel extends Model
{
    protected $name = 'ad_position';
    // 开启自动写入时间戳
    protected $autoWriteTimestamp = true;

    /**
     * 根据条件获取全部数据
     *
     * @param $nowpage
     * @param $limits
     *
     * @return false|\PDOStatement|string|\think\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @author   [小码哥]
     */
    public function getAll($nowpage, $limits)
    {
        return $this->page($nowpage, $limits)->order('id asc')->select();
    }

    /**
     * 插入信息
     *
     * @param $param
     *
     * @return array
     * @author   [小码哥]
     */
    public function insertAdPosition($param)
    {
        try{
            $result = $this->validate('AdPositionValidate')->allowField(true)->save($param);
            if(false === $result){
                return ['code' => -1, 'data' => '', 'msg' => $this->getError()];
            }else{
                return ['code' => 1, 'data' => '', 'msg' => '添加广告位成功'];
            }
        }catch(PDOException $e){
            return ['code' => -2, 'data' => '', 'msg' => $e->getMessage()];
        }
    }

    /**
     * 编辑信息
     *
     * @param $param
     *
     * @return array
     * @author   [小码哥]
     */
    public function editAdPosition($param)
    {
        try{
            $result = $this->validate('AdPositionValidate')->allowField(true)->save($param, ['id' => $param['id']]);
            if(false === $result){
                return ['code' => 0, 'data' => '', 'msg' => $this->getError()];
            }else{
                return ['code' => 1, 'data' => '', 'msg' => '编辑广告位成功'];
            }
        }catch(PDOException $e){
            return ['code' => 0, 'data' => '', 'msg' => $e->getMessage()];
        }
    }

    /**
     * 根据id获取一条信息
     *
     * @param $id
     *
     * @return array|false|\PDOStatement|string|Model
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @author   [小码哥]
     */
    public function getOne($id)
    {
        return $this->where('id', $id)->find();
    }

    /**
     * 获取全部广告位
     *
     * @return false|\PDOStatement|string|\think\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @author   [小码哥]
     */
    public function getAllPosition()
    {
        return $this->order('id asc')->where('status', 1)->select();
    }

    /**
     * 删除信息
     *
     * @param $id
     *
     * @return array
     * @author   [小码哥]
     */
    public function delAdPosition($id)
    {
        try{
            $this->where('id', $id)->delete();
            return ['code' => 1, 'data' => '', 'msg' => '删除广告位成功'];
        }catch(PDOException $e){
            return ['code' => 0, 'data' => '', 'msg' => $e->getMessage()];
        }
    }
}