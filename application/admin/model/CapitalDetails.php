<?php
/**
 * Created by PhpStorm.
 * User: PHP
 * Date: 2018/1/18
 * Time: 15:28
 */
namespace app\admin\model;

use think\Model;

class CapitalDetails extends Model
{
    protected $autoWriteTimestamp = true;

    /**
     * 获取充值记录
     *
     * @param array $where
     * @param int   $page
     * @param int   $limit
     *
     * @return false|\PDOStatement|string|\think\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @author   [小码哥]
     */
    public function getLists($where = [], $page = 1, $limit = 10)
    {
        $map = [];
        foreach($where as $index => $item){
            $map["a.{$index}"] = $item;
        }
        return $this->alias('a')
            ->join(['__MEMBER__' => 'b'], 'a.account=b.account')
            ->join(['__MEMBER_GROUP__' => 'c'], 'c.id=b.group_id')
            ->join(['__ADMIN__' => 'd'], 'd.id=a.admin_id')
            ->join(['__AUTH_GROUP__' => 'e'], 'e.id=d.groupid')
            ->field(['a.*', 'b.nickname' => 'name', 'b.sex', 'b.head_img', 'b.reg_integral', 'b.gouwu_integral', 'b.gouche_integral', 'c.group_name', 'd.username', 'e.title' => 'admin_group_name'])
            ->where($map)
            ->page($page, $limit)
            ->select();
    }

    /**
     * 转换性别
     *
     * @param $value
     *
     * @return mixed
     * @author   [小码哥]
     */
    public function getSexAttr($value)
    {
        $sex = ['保密', '男', '女'];
        return $sex[$value];
    }

    public function getTypeAttr($value)
    {
        $type = ['reg_integral' => '注册积分', 'gouwu_integral' => '购物积分', 'gouche_integral' => '购车积分'];
        return $type[$value];
    }
}