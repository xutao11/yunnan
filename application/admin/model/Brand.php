<?php
/**
 * Created by PhpStorm.
 * User: PHP
 * Date: 2018/1/29
 * Time: 11:14
 */
namespace app\admin\model;

use think\exception\PDOException;
use think\Model;

class Brand extends Model
{
    protected $autoWriteTimestamp = true;

    /**
     * 获取品牌
     *
     * @param array $where
     * @param int   $page
     * @param int   $limit
     *
     * @return false|\PDOStatement|string|\think\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @author   [小码哥]
     */
    public function getBrand($where = [], $page = 1, $limit = 10)
    {
        return $this->where($where)->page($page, $limit)->select();
    }

    /**
     * 添加品牌
     *
     * @param array $param
     *
     * @return array
     * @author   [小码哥]
     */
    public function brandAdd($param = [])
    {
        try{
            $result = $this->allowField(true)->save($param);
            if($result){
                return ['code' => 200, 'data' => null, 'msg' => '添加成功'];
            }
            return ['code' => 0, 'data' => null, 'msg' => '添加失败'];
        }catch(PDOException $e){
            return ['code' => 0, 'data' => null, 'msg' => $e->getMessage()];
        }
    }

    /**
     * 获取单条记录
     *
     * @param $brand_id
     *
     * @return array|false|\PDOStatement|string|Model
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @author   [小码哥]
     */
    public function getItem($brand_id)
    {
        return $this->where('brand_id', $brand_id)->find();
    }

    /**
     * 编辑
     *
     * @param array $param
     *
     * @return array
     * @author   [小码哥]
     */
    public function brandEdit($param = [])
    {
        try{
            $result = $this->allowField(true)->save($param, ['brand_id' => $param['brand_id']]);
            if($result){
                return ['code' => 200, 'data' => null, 'msg' => '编辑成功'];
            }
            return ['code' => 0, 'data' => null, 'msg' => '编辑失败'];
        }catch(PDOException $e){
            return ['code' => 0, 'data' => null, 'msg' => $e->getMessage()];
        }
    }
}